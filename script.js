class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get _name() {
    return this.name;
  }
  get _age() {
    return this.age;
  }
  get _salary() {
    return this.salary;
  }

  set _name(value) {
    this.name = value;
  }
  set _age(value) {
    this.age = value;
  }
  set _salary(value) {
    this.salary = value;
  }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }

    get _salary (){
        return this.salary*3;
    }
}


const programmer1 = new Programmer('Sarah', '26', 300, ['C#', 'JavaScript']);
const programmer2 = new Programmer('Salah', '29', 20000, ['Python', 'JavaScript']);
const programmer3 = new Programmer('Sameeh', '34', 40000, ['Python', 'NextJS', 'Go']);

console.log(programmer1);
programmer2.salary = 5000;
console.log(programmer2);
console.log(programmer3);
